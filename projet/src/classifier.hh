#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "corners.hh"

std::vector<std::pair<std::string, cv::Rect>>
classify(cv::dnn::Net& net, cv::String& img_path);
