#include "corners.hh"

cv::Mat equalizeIntensity(const cv::Mat& inputImage)
{
    if(inputImage.channels() >= 3)
    {
        cv::Mat ycrcb;

        cv::cvtColor(inputImage, ycrcb, cv::COLOR_BGR2YCrCb);

        std::vector<cv::Mat> channels;
        cv::split(ycrcb, channels);

        cv::equalizeHist(channels[0], channels[0]);

        cv::Mat result;
        cv::merge(channels, ycrcb);

        cv::cvtColor(ycrcb, result, cv::COLOR_YCrCb2BGR);

        return result;
    }
    return cv::Mat();
}

void BrightnessAndContrastAuto(const cv::Mat &src, cv::Mat &dst, float clipHistPercent=0)
{

    CV_Assert(clipHistPercent >= 0);
    CV_Assert((src.type() == CV_8UC1) || (src.type() == CV_8UC3) || (src.type() == CV_8UC4));

    int histSize = 256;
    float alpha, beta;
    double minGray = 0, maxGray = 0;

    //to calculate grayscale histogram
    cv::Mat gray;
    if (src.type() == CV_8UC1) gray = src;
    else if (src.type() == CV_8UC3) cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    else if (src.type() == CV_8UC4) cvtColor(src, gray, cv::COLOR_BGRA2GRAY);
    if (clipHistPercent == 0)
    {
        // keep full available range
        cv::minMaxLoc(gray, &minGray, &maxGray);
    }
    else
    {
        cv::Mat hist; //the grayscale histogram

        float range[] = { 0, 256 };
        const float* histRange = { range };
        bool uniform = true;
        bool accumulate = false;
        calcHist(&gray, 1, 0, cv::Mat (), hist, 1, &histSize, &histRange, uniform, accumulate);

        // calculate cumulative distribution from the histogram
        std::vector<float> accumulator(histSize);
        accumulator[0] = hist.at<float>(0);
        for (int i = 1; i < histSize; i++)
        {
            accumulator[i] = accumulator[i - 1] + hist.at<float>(i);
        }

        // locate points that cuts at required value
        float max = accumulator.back();
        clipHistPercent *= (max / 100.0); //make percent as absolute
        clipHistPercent /= 2.0; // left and right wings
        // locate left cut
        minGray = 0;
        while (accumulator[minGray] < clipHistPercent)
            minGray++;

        // locate right cut
        maxGray = histSize - 1;
        while (accumulator[maxGray] >= (max - clipHistPercent))
            maxGray--;
    }

    // current range
    float inputRange = maxGray - minGray;

    alpha = (histSize - 1) / inputRange;   // alpha expands current range to histsize range
    beta = -minGray * alpha;             // beta shifts current range so that minGray will go to 0

    // Apply brightness and contrast normalization
    // convertTo operates with saurate_cast
    src.convertTo(dst, -1, alpha, beta);

    // restore alpha channel from source 
    if (dst.type() == CV_8UC4)
    {
        int from_to[] = { 3, 3};
        cv::mixChannels(&src, 4, &dst,1, from_to, 1);
    }
    return;
}


std::vector<cv::Point> get_corners(cv::Mat& img, const cv::Rect& region) {
  std::vector<cv::Point> res;
  cv::Mat reg = img(region);
  //reg = equalizeIntensity(reg);
  cv::Mat gray, dgray;
  cv::cvtColor(reg, gray, cv::COLOR_BGR2GRAY);
  cv::fastNlMeansDenoising(gray, gray, 20);
  BrightnessAndContrastAuto(gray, gray);
  //cv::normalize(gray, gray, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());

  //cv::blur(gray, gray, cv::Size(3,3));
  //cv::equalizeHist(gray, gray);

  int imgsize = reg.cols > reg.rows ? reg.rows : reg.cols;

  cv::Canny(gray, gray, 50, 200, 3);
  cv::Mat kernel = (cv::Mat_<uchar>(3,3) << 1,1,1,1,1,1,1,1,1);
  cv::dilate(gray, gray, kernel);
  cv::cvtColor(gray, dgray, cv::COLOR_GRAY2BGR);

  std::vector<cv::Vec4i> lines;
  cv::HoughLinesP(gray, lines, 1, CV_PI/180, 0.2*imgsize, 0.2*imgsize, 0.02*imgsize);
  cv::Mat comp(reg.size(), CV_64FC1, cv::Scalar(0)), thr;
  for( size_t i = 0; i < lines.size(); i++ ) {
    cv::Vec4i l = lines[i];
    cv::line(reg, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0,0,255), 3, cv::LINE_AA);
    cv::line(comp, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(255), 3, cv::LINE_AA);
  }
  cv::threshold(comp, thr, 100, 255, cv::THRESH_BINARY);
  cv::Moments m = cv::moments(thr, true);
  cv::circle(reg, cv::Point(m.m10/m.m00, m.m01/m.m00), 20, cv::Scalar(128,0,0), -1);
  cv::namedWindow("corners" , cv::WINDOW_NORMAL);
  cv::imshow("corners" , reg);
  cv::waitKey(0);
  return res;
}

cv::Point get_center(const std::vector<cv::Point>& points) {
  int x = 0;
  int y = 0;
  int c = 0;
  for (cv::Point p : points) {
    x += p.x;
    y += p.y;
    c += 1;
  }
  cv::Point res(x/c, y/c);
  return res;
}
