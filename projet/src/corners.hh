#pragma once

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

std::vector<cv::Point> get_corners(cv::Mat& img, const cv::Rect& region);
cv::Point get_center(const std::vector<cv::Point>& points);
