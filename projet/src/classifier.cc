#include "classifier.hh"

std::vector<std::pair<std::string, cv::Rect>>
classify(cv::dnn::Net& net, cv::String& img_path)
{
    std::vector<std::pair<std::string, cv::Rect>> ans;
    cv::Mat img = cv::imread(img_path, cv::IMREAD_COLOR);
    cv::Mat input_blob = cv::dnn::blobFromImage(img, 1 / 255.f,
            cv::Size(416, 416), cv::Scalar(), true, false);
    net.setInput(input_blob);

    std::vector<cv::String> out({"yolo_82", "yolo_94", "yolo_106"});

    for (auto layer : out)
    {
        cv::Mat detection_mat = net.forward(layer);
        float conf_threshold = 0.2f;

        for (int i = 0; i < detection_mat.rows; i++)
        {
            const int proba_index = 5;
            const int proba_size = detection_mat.cols - proba_index;
            float* prob_array_ptr = &detection_mat.at<float>(i, proba_index);

            size_t object_class = std::max_element(prob_array_ptr,
                    prob_array_ptr + proba_size) - prob_array_ptr;
            float conf = detection_mat.at<float>(i,
                    (int) object_class + proba_index);
            if (conf > conf_threshold)
            {
                float x = detection_mat.at<float>(i, 0);
                float y = detection_mat.at<float>(i, 1);
                float w = detection_mat.at<float>(i, 2);
                float h = detection_mat.at<float>(i, 3);
                float max = h > w ? h : w;

                int xmax = max * img.cols;
                int ymax = max * img.rows * 1.5;

                int xpix = static_cast<int>((x - max / 2) * img.cols);
                int ypix = static_cast<int>((y - ( max* 1.5 ) / 2) * img.rows);
                xpix = xpix > 0 ? xpix : 0;
                xpix = xpix < img.cols ? xpix : img.cols-1;
                ypix = ypix > 0 ? ypix : 0;
                ypix = ypix < img.rows ? ypix : img.rows-1;
                max = max > 0 ? max : 0;
                xmax = xpix + xmax < img.cols ? xmax : img.cols - xpix - 1;
                ymax = ypix + ymax < img.rows ? ymax : img.rows - ypix - 1;

                std::string s = object_class == 0 ? "damier" : "mire";

                cv::Rect rect(xpix, ypix, xmax, ymax);
                get_corners(img, rect);

                cv::putText(img, s, cv::Point(xpix, ypix-(0.01*img.rows)), 0, 3, 2, 8, 0);
                cv::rectangle(img, rect, cv::Scalar(0,255,0) , 10, 8, 0 );
                
                ans.push_back(std::make_pair(s, rect));
            }
        }
    }

    
    cv::namedWindow(img_path, cv::WINDOW_NORMAL);
    cv::imshow(img_path, img);
    cv::waitKey(0);
    cv::destroyWindow(img_path);
    
    return ans;
}
