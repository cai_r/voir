#include <iostream>

#include "classifier.hh"

using v_results = std::vector<std::pair<std::string, cv::Rect>>;
using result = std::pair<std::string, cv::Rect>;

int main(int argc, char** argv)
{
    if (argc != 4)
    {
        std::cerr << "Usage: ./main [path_to_cfg] [path_to_weights] "
            << "[path_to_folder]\n";
        return -1;
    }

    cv::String cfg(argv[1]);
    cv::String weights(argv[2]);
    cv::String folder(argv[3]);
    std::vector<cv::String> images;
    cv::glob(folder, images, true);
    cv::dnn::Net net = cv::dnn::readNetFromDarknet(cfg, weights);

    if (net.empty())
    {
        std::cerr << "Model could not be loaded.\n";
        return -1;
    }

    for (cv::String s : images)
    {
        v_results coord = classify(net, s);
        std::cout << s << " - nb object: " << coord.size() << "\n";
        for (result p : coord)
            std::cout << p.first << ": " << p.second << "\n";
    }

    return 0;
}
